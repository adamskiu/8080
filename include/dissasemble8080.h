#ifndef DISSASEMBLE8080_H
#define DISSASEMBLE8080_H
#include <stdio.h>
int Dissasemble8080(unsigned char *codebuffer, int pc);
void print_instruction(unsigned char opcode);
#endif // DISSASEMBLE8080_H

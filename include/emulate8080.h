#ifndef EMULATE8080_H
#define EMULATE8080_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define MSB          0x80   // Most Significant Bit
#define FIRST_8_BITS 0xff   // Zapalone wszystkie bity w bajcie
#define FIRST_16_BITS 0xffff

// W 8080 to był ośmiobitowy rejestr o nazwie F
typedef struct ConditionCodes {     // flagi procesora
    uint8_t    z:1;                 //            zero flag
    uint8_t    s:1;                 //            sign flag
    uint8_t    p:1;                 //          parity flag
    uint8_t    cy:1;                //           carry flag
    uint8_t    ac:1;                // auxillary carry flag
    uint8_t    padding:3;
} ConditionCodes;

// Rzeczywisty rozkład:
// 7 6 5 4 3 2 1 0
// S Z 0 A 0 P 1 C   -- Sign, Zero, 0, AC, 0, Parity, 1, Carry
// tam gdzie zamiast nazw flag podano stałe znajdują się
// Nieużywane miejsca i zawsze mają takie wartości

/* Flagi:
*   zero            - Ustawia się gdy wynik działania jest równy 0
*   sign            - Ustawia się gdy bit 7 (MSB - Most Significant Bit) jest równy 1
*                     To dzieje się przy instrukcjach matematycznych
*   parity          - Ustawia się gdy wynik ma ustawioną parzystą liczbę bitów.
*   carry           - Set to 1 when the instruction resulted in a carry out
*                     or borrow into the high order bit (Przetłumacz)
*   auxillary carry - Used mostly for BCD (binary coded decimal) math.
*                     Read the data book for more details, Space Invaders doesn't use it.
*/

typedef struct State8080 {
    uint8_t    A;       // accumulator
    uint8_t    B;       // general purpose
    uint8_t    C;       // general purpose
    uint8_t    D;       // general purpose
    uint8_t    E;       // general purpose
    uint8_t    H;       // general purpose, często używany z L
    uint8_t    L;       // general purpose
    uint16_t   SP;      // stack pointer
    uint16_t   PC;      // program counter, instruction pointer
    uint8_t    *memory;
    struct     ConditionCodes      flags;
    uint8_t    int_enable;
} State8080;

void Emulate8080Op(State8080* state);
int parity (uint16_t number);
int check_ac_add(uint16_t number1, uint16_t number2);
int check_ac_adc(uint16_t number1, uint16_t number2, uint16_t carry);
int check_ac_sub(uint16_t number1, uint16_t number2);
void print_state(State8080 state);
#endif // HELP_FUNCTIONS_H

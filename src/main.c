#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "dissasemble8080.h"
#include "emulate8080.h"

/*
 * kolejność sklejania roma:
 *    cat invaders.h > invaders
      cat invaders.g >> invaders
      cat invaders.f >> invaders
      cat invaders.e >> invaders
*/

State8080 CPU;

int main(int argcount, char **argvector)
{
	FILE *file = fopen(argvector[1], "rb");
	if (file == NULL){
		printf("Could not open %s\n", argvector[1] );
		exit(1);
	}

	// Get the file size and read it into a memory buffer
	fseek(file, 0L, SEEK_END);
	int fsize = ftell(file);
	fseek(file, 0L, SEEK_SET);

	unsigned char *buffer = malloc(fsize);

	fread(buffer, fsize, 1, file);
	fclose(file);

	CPU.memory = malloc(65536);
	memset(CPU.memory, 0, 65536);	
	// ROM powinien zacząć się w adresie 0x8000! (a może nie?)
	memcpy(CPU.memory, buffer, fsize);	// skopiuj roma do pamięci procesora
	free(buffer);

	int pc = 0;

	printf("Rozmiar roma: %d\n", fsize);
	while (1){
		Emulate8080Op(&CPU);
	}

	while (pc < fsize){
		pc += Dissasemble8080(buffer, pc);
		printf("\n");
	}
	return 0;
}

#include "emulate8080.h"
#include "dissasemble8080.h"
void UnimplementedInstruction(State8080* state)
{
    //pc zwiększy się o 1, trzeba zwrócić na to uwagę
    state->PC -= 1;
    printf ("Error: Unimplemented instruction: ");
    print_instruction(state->memory[state->PC]);
    printf(" (opcode 0x%02x)\n", state->memory[state->PC]);
    exit(1);
}

void Emulate8080Op(State8080* state)
{
    unsigned char *opcode = &state->memory[state->PC];
    uint16_t result = 0;
    uint16_t offset = 0;
    printf("Do wykonania ");
    Dissasemble8080(state->memory, state->PC);
    printf("\n");

    state->PC+=1;  // pobranie instrukcji, to pobranie jednego bajta
    switch(*opcode){
        case 0x00: break;   // NOP - 4 cykle
        case 0x01:  // LXI B,D16 - 10 cykli
            state->C = opcode[1];
            state->B = opcode[2];
            state->PC += 2;
        break;
        case 0x02:  // STAX B - 7 cykli
            offset = (state->B << 8) | (state->C);
            state->memory[offset] = state->A; 
        break;
        case 0x03:  // INX B - 5 cykli
            result = (state->B << 8) | (state->C);
            result += 1;    
            state->B = (result >> 8) & FIRST_8_BITS;
            state->C = result & FIRST_8_BITS;
        break;
        case 0x04:  // INR B - 5 cykli
            result = state->B + 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0); 
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->B, 1);
            state->B = result & FIRST_8_BITS;
        break;
        case 0x05:  // DCR B - 5 cykli
            result = state->B - 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_sub(state->B, 1);
            state->B = result & FIRST_8_BITS; 
        break;
        case 0x06:      // MVI B,D8 - 7 cykli
            state->B = opcode[1];
            state->PC += 1;
        break;
        case 0x07:  // RLC - 4 cykle
            result = state->A;
            state->flags.cy = ((state->A & MSB) != 0);
            result = result << 1;
            if (state->flags.cy == 1)
                result = result | 1;
            else
                result = result & 0xfe;
            state->A = result & FIRST_8_BITS;
        break;
        case 0x08: break;       // NOP - 4 cykle
        case 0x09:  // DAD B - 10 cykli
        {
            uint16_t number1 = (state->B << 8) | (state->C);
            uint16_t number2 = (state->H << 8) | (state->L);
            uint32_t result32 = number1 + number2;
            state->flags.cy = (result32 > FIRST_16_BITS);
            state->H = (result32 & 0xff00) >> 8;
            state->L = result32 & FIRST_8_BITS; 
        }  
        break;
        case 0x0a:  // LDAX B - 7 cykli
            offset = (state->B << 8) | (state->C);
            state->A = state->memory[offset];  
        break;
        case 0x0b:  // DCX B - 5 cykli
            result = (state->B << 8) | (state->C);
            result -= 1;
            state->B = (result >> 8) & FIRST_8_BITS;
            state->C = result & FIRST_8_BITS;
        break;
        case 0x0c:  // INR C - 5 cykli
            result = state->C + 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0); 
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->C, 1);
            state->C = result & FIRST_8_BITS; 
        break;
        case 0x0d:  // DRC C - 5 cykli
            result = state->C - 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_sub(state->C, 1);
            state->C = result & FIRST_8_BITS;  
        break;
        case 0x0e:  // MVI C,D8 - 7 cykli
            state->C = opcode[1];
            state->PC += 1;
        break;
        case 0x0f:  // RRC - 4 cykle 
            result = state->A;
            state->flags.cy = ((state->A & 0x01) != 0);
            result = result >> 1;
            if (state->flags.cy == 1)
                result = result | MSB;
            else
                result = result & 0x7f;
            state->A = result & FIRST_8_BITS;
        break;

        case 0x10: break;       // NOP - 4 cykle
        case 0x11:      // LXI D,D16 - 10 cykli
            state->E = opcode[1];
            state->D = opcode[2];
            state->PC += 2; 
        break;
        case 0x12:  // STAX D - 7 cykli
            offset = (state->D << 8) | (state->E);
            state->memory[offset] = state->A;  
        break;
        case 0x13:  // INX D - 5 cykli
            result = (state->D << 8) | (state->E); 
            result += 1;
            state->D = (result >> 8) & FIRST_8_BITS;
            state->E = result & FIRST_8_BITS;
        break;
        case 0x14:  // INR D - 5 cykli
            result = state->D + 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0); 
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->D, 1);
            state->D = result & FIRST_8_BITS;  
        break;
        case 0x15:  // DCR D - 5 cykli
            result = state->D - 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_sub(state->D, 1);
            state->D = result & FIRST_8_BITS;  
        break;
        case 0x16:  // MVI D,D8 - 7 cykli
            state->D = opcode[1];
            state->PC += 1; 
        break;
        case 0x17:  // RAL - 4 cykle
            result = state->A;
            result = result << 1;
            if (state->flags.cy == 1)
                result = result | 1;
            else
                result = result & 0x1fe;
            state->flags.cy = ((result & 0x100) != 0);
            state->A = result & 0xff;
        break;
        case 0x18: break;       // NOP - 4 cykle
        case 0x19:  // DAD D - 10 cykli
        {
            uint16_t number1 = (state->D << 8) | (state->E);
            uint16_t number2 = (state->H << 8) | (state->L);
            uint32_t result32 = number1 + number2;
            state->flags.cy = (result32 > FIRST_16_BITS);
            state->H = (result32 & 0xff00) >> 8;
            state->L = result32 & FIRST_8_BITS; 
        }   
        break;
        case 0x1a:  // LDAX D - 7 cykli
            offset = (state->D << 8) | (state->E);
            state->A = state->memory[offset];
        break;
        case 0x1b:  // DCX D - 5 cykli
            result = (state->D << 8) | (state->E);
            result -= 1;
            state->D = (result >> 8) & FIRST_8_BITS;
            state->E = result & FIRST_8_BITS;
        break;
        case 0x1c:  // INR E - 5 cykli
            result = state->E + 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0); 
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->E, 1);
            state->E = result & FIRST_8_BITS;  
        break;
        case 0x1d:  // DCR E - 5 cykli
            result = state->E - 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_sub(state->E, 1);
            state->E = result & FIRST_8_BITS;   
        break;
        case 0x1e: // MVI E,D8 - 7 cykli
            state->E = opcode[1];
            state->PC += 1;
        break;
        case 0x1f:  // RAR - 4 cykle
            result = state->A;
            uint8_t lsb = ((result & 0x01) != 0); // least significant bit
            result = result >> 1;
            if (state->flags.cy == 1)
                result = result | MSB;
            else
                result = result & 0x7f;
            state->flags.cy = lsb;
            state->A = result & 0xff; 
        break;

        case 0x20: break;       // NOP - 4 cykle
        case 0x21:  // LXI H,D16 - 10 cykli
            state->L = opcode[1];
            state->H = opcode[2];
            state->PC += 2; 
        break;
        case 0x22:  // SHLD a16 - 16 cykli
            offset = (opcode[2] << 8) | (opcode[1]);
            state->memory[offset] = state->L;
            state->memory[offset + 1] = state->H;
            state->PC += 2; 
        break;
        case 0x23:  // INX H - 5 cykli
            result = (state->H << 8) | (state->L);
            result += 1;    
            state->H = (result >> 8) & FIRST_8_BITS;
            state->L = result & FIRST_8_BITS;
        break;
        case 0x24:  // INR H - 5 cykli
            result = state->H + 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0); 
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->H, 1);
            state->H = result & FIRST_8_BITS;
        break;
        case 0x25:  // DCR H - 5 cykli
            result = state->H - 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_sub(state->H, 1);
            state->H = result & FIRST_8_BITS;  
        break;
        case 0x26:  // MVI H,D8 - 7 cykli
            state->H = opcode[1];
            state->PC += 1; 
        break;
        case 0x27:  // DAA - 4 cykle
        {
            // least significant four bits
            // of the accumulator 
            uint8_t ls_digit = state->A & 0x0f;
            if (ls_digit > 9 || state->flags.ac == 1){
                state->A += 6;
                state->flags.ac = check_ac_add(ls_digit, 6);
            }
            else state->flags.ac = 0;
                    
            // most significant four bits
            // of the accumulator
            uint8_t ms_digit = (state->A >> 4) & 0x0f;
            ls_digit = state->A & 0x0f;
            if (ms_digit > 9 || state->flags.cy == 1){
                state->flags.cy = check_ac_add(ms_digit, 6);
                ms_digit += 6;
            }
            // carry is unaffected if no increment occurs
            // złóż obydwie cyfry czterobitowe
            state->A = (ms_digit << 4) | ls_digit;
            state->flags.z = ((state->A & FIRST_8_BITS) == 0);
            state->flags.p = parity(state->A);
            state->flags.s = ((state->A & MSB) != 0);
        }
        break;
        case 0x28: break;       // NOP - 4 cykle
        case 0x29:  // DAD H - 10 cykli
        {
            uint16_t number1 = (state->H << 8) | (state->L);
            uint16_t number2 = number1;
            uint32_t result32 = number1 + number2;
            state->flags.cy = (result32 > FIRST_16_BITS);
            state->H = (result32 & 0xff00) >> 8;
            state->L = result32 & FIRST_8_BITS; 
        }    
        break;
        case 0x2a:  // LHLD a16 - 16 cykli
            offset = (opcode[2] << 8) | (opcode[1]);
            state->L = state->memory[offset];
            state->H = state->memory[offset + 1];
            state->PC += 2; 
        break;
        case 0x2b:  // DCX H - 5 cykli
            result = (state->H << 8) | (state->L);
            result -= 1;
            state->H = (result >> 8) & FIRST_8_BITS;
            state->L = result & FIRST_8_BITS; 
        break;
        case 0x2c:  // INR L - 5 cykli
            result = state->L + 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0); 
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->L, 1);
            state->L = result & FIRST_8_BITS;  
        break;
        case 0x2d:  // DRC L - 5 cykli
            result = state->L - 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_sub(state->L, 1);
            state->L = result & FIRST_8_BITS;   
        break;
        case 0x2e:  // MVI L,D8 - 7 cykli
            state->L = opcode[1];
            state->PC += 1; 
        break;
        case 0x2f:  // CMA - 4 cykle
            state->A = ~state->A;
        break;

        case 0x30: break;       // NOP - 4 cykle
        case 0x31:  // LXI SP, D16 - 10 cykli
            result = (opcode[2]<< 8) | (opcode[1]);
            state->SP = result;
            state->PC += 2;
        break;
        case 0x32:  // STA a16 - 13 cykli
            offset = (opcode[2] << 8) | (opcode[1]);
            state->memory[offset] = state->A;
            state->PC += 2;  
        break;
        case 0x33: state->SP += 1;  // INX SP - 5 cykli 
        break;
        case 0x34:  // INR M - 10 cykli
            offset =  (state->H << 8) | (state->L);
            result = state->memory[offset] + 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0); 
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->memory[offset], 1);
            state->memory[offset] = result & FIRST_8_BITS;
        break;
        case 0x35:  // DCR M - 10 cykli
            offset = (state->H << 8) | (state->L);
            result = state->memory[offset] - 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_sub(state->memory[offset], 1);
            state->memory[offset] = result & FIRST_8_BITS;  
        break;
        case 0x36:  // MVI M,D8 - 10 cykli
        //AC set if lower nibble of h was zero prior to dec - może tak być
            offset = (state->H << 8) | (state->L);
            state->memory[offset] = opcode[1];
            state->PC += 1; 
        break;
        case 0x37: state->flags.cy = 1; break; // STC - 4 cykle
        case 0x38: break;       // NOP - 4 cykle
        case 0x39:  // DAD SP - 10 cykli
        {
            uint16_t number1 = state->SP;
            uint16_t number2 = (state->H << 8) | (state->L);
            uint32_t result32 = number1 + number2;
            state->flags.cy = (result32 > FIRST_16_BITS);
            state->H = (result32 & 0xff00) >> 8;
            state->L = result32 & FIRST_8_BITS; 
        }   
        break;
        case 0x3a:  // LDA a16 - 13 cykli
            offset = (opcode[2] << 8) | (opcode[1]);
            state->A = state->memory[offset];
            state->PC += 2;  
        break;
        case 0x3b: state->SP += 1; break; // DCX SP - 5 cykli
        case 0x3c:  // INR A - 5 cykli
            result = state->A + 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0); 
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->A, 1);
            state->A = result & FIRST_8_BITS;   
        break;
        case 0x3d:  // DCR A - 5 cykli
            result = state->A - 1;
            state->flags.s = ((result & MSB) != 0);
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_sub(state->A, 1);
            state->A = result & FIRST_8_BITS;   
        break;
        case 0x3e:  // MVI A,D8 - 7 cykli
            state->A = opcode[1];
            state->PC += 1; 
        break;
        case 0x3f:  // CMC - 4 cykle
        // Complement carry flag
        // jeśli flaga jest == 1 to ustawia ją na 0
        // jeśli flaga jest == 0 to ustawia ją na 1
            state->flags.cy = (state->flags.cy == 0); 
        break;

        // Wszystkie instrukcje z grupy MOV zajmują 5 cykli!
        // (oprócz MOVów do pamięci )
        case 0x40: state->B = state->B; break;  // MOV B,B
        case 0x41: state->B = state->C; break;  // MOV B,C
        case 0x42: state->B = state->D; break;  // MOV B,D
        case 0x43: state->B = state->E; break;  // MOV B,E
        case 0x44: state->B = state->H; break;  // MOV B,H
        case 0x45: state->B = state->L; break;  // MOV B,L
        case 0x46:  // MOV B,M - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->B = state->memory[offset];
        break;
        case 0x47: state->B = state->A; break;  // MOV B,A

        case 0x48: state->C = state->B; break;  // MOV C,B
        case 0x49: state->C = state->C; break;  // MOV C,C
        case 0x4a: state->C = state->D; break;  // MOV C,D
        case 0x4b: state->C = state->E; break;  // MOV C,E
        case 0x4c: state->C = state->H; break;  // MOV C,H
        case 0x4d: state->C = state->L; break;  // MOV C,L
        case 0x4e:  // MOV C,M - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->C = state->memory[offset];
        break;
        case 0x4f: state->C = state->A; break;  // MOV C,A

        case 0x50: state->D = state->B; break;  // MOV D,B
        case 0x51: state->D = state->C; break;  // MOV D,C
        case 0x52: state->D = state->D; break;  // MOV D,D
        case 0x53: state->D = state->E; break;  // MOV D,E
        case 0x54: state->D = state->H; break;  // MOV D,H
        case 0x55: state->D = state->L; break;  // MOV D,L
        case 0x56:  // MOV D,M - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->D = state->memory[offset];
        break;
        case 0x57: state->D = state->A; break;  // MOV D,A

        case 0x58: state->E = state->B; break;  // MOV E,B
        case 0x59: state->E = state->C; break;  // MOV E,C
        case 0x5a: state->E = state->D; break;  // MOV E,D
        case 0x5b: state->E = state->E; break;  // MOV E,E
        case 0x5c: state->E = state->H; break;  // MOV E,H
        case 0x5d: state->E = state->L; break;  // MOV E,L
        case 0x5e:  // MOV E,M - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->E = state->memory[offset];
        break;
        case 0x5f: state->E = state->A; break;  // MOV E,A

        case 0x60: state->H = state->B; break;  // MOV H,B
        case 0x61: state->H = state->C; break;  // MOV H,C
        case 0x62: state->H = state->D; break;  // MOV H,D
        case 0x63: state->H = state->E; break;  // MOV H,E
        case 0x64: state->H = state->H; break;  // MOV H,H
        case 0x65: state->H = state->L; break;  // MOV H,L
        case 0x66:  // MOV H,M - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->H = state->memory[offset];
        break;
        case 0x67: state->H = state->A; break;  // MOV H,A

        case 0x68: state->L = state->B; break;  // MOV L,B
        case 0x69: state->L = state->C; break;  // MOV L,C
        case 0x6a: state->L = state->D; break;  // MOV L,D
        case 0x6b: state->L = state->E; break;  // MOV L,E
        case 0x6c: state->L = state->H; break;  // MOV L,H
        case 0x6d: state->L = state->L; break;  // MOV L,L
        case 0x6e:  // MOV L,M - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->L = state->memory[offset];
        break;

        case 0x6f: state->L = state->A; break;  // MOV L,A

        case 0x70:  // MOV M,B - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->memory[offset] = state->B;
        break;

        case 0x71:  // MOV M,C - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->memory[offset] = state->C;
        break;

        case 0x72:  // MOV M,D - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->memory[offset] = state->D;
        break;

        case 0x73:  // MOV M,E - 7 cykli
             offset = (state->H << 8) | (state->L);
             state->memory[offset] = state->E;
        break;

        case 0x74:  // MOV M,H - 7 cykli
             offset = (state->H << 8) | (state->L);
             state->memory[offset] = state->H;
        break;

        case 0x75: // MOV M,L - 7 cykli
             offset = (state->H << 8) | (state->L);
             state->memory[offset] = state->L;
        break;

        case 0x76: UnimplementedInstruction(state); break;  // HLT 7 cykli

        case 0x77:  // MOV M,A - 7 cykli
            offset = (state->H << 8) | (state->L);
            state->memory[offset] = state->A;
        break;

        case 0x78: state->A = state->B; break;  // MOV A,B
        case 0x79: state->A = state->C; break;  // MOV A,C
        case 0x7a: state->A = state->D; break;  // MOV A,D
        case 0x7b: state->A = state->E; break;  // MOV A,E
        case 0x7c: state->A = state->H; break;  // MOV A,H
        case 0x7d: state->A = state->L; break;  // MOV A,L

        case 0x7e:  // MOV A,M
            offset = (state->H << 8) | (state->L);
            state->A = state->memory[offset];
        break;

        case 0x7f: state->A = state->A; break;  // MOV A,A

        case 0x80:  // ADD B - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->B;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
			state->flags.ac = check_ac_add(state->A, state->B);
            state->A = result & FIRST_8_BITS;   
        break;

        case 0x81:  // ADD C - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->C;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->A, state->C);
            state->A = result & FIRST_8_BITS;
        break;

        case 0x82:  // ADD D - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->D;
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.s = ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->A, state->D);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->A = result & FIRST_8_BITS;
        break;

        case 0x83:  // ADD E - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->E;
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.s = ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->A, state->E);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->A = result & FIRST_8_BITS;
        break;

        case 0x84:  // ADD H - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->H;
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.s = ((result & MSB) != 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->A, state->H);
            state->A = result & FIRST_8_BITS;
        break;

        case 0x85:  // ADD L - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->L;
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.s = ((result & MSB) != 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->A, state->L);
            state->A = result & FIRST_8_BITS;
        break;

        case 0x86:  // ADD M - 7 cykli
            offset = (state->H << 8) | (state->L);
            result = (uint16_t) state->A + (uint16_t) state->memory[offset];
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.s = ((result & MSB) != 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->A, state->memory[offset]);
            state->A = result & FIRST_8_BITS;
        break;
        case 0x87:  // ADD A - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->A;
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.s = ((result & MSB) != 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.ac = check_ac_add(state->A, state->A);
            state->A = result & FIRST_8_BITS;
        break;

        case 0x88:  // ADC B - 4 cykle 
            result = (uint16_t) state->A + (uint16_t) state->B + (uint16_t) state->flags.cy;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_adc(state->A, state->B, state->flags.cy);
            state->A = result & FIRST_8_BITS;
        break;
        case 0x89:  // ADC C - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->C + (uint16_t) state->flags.cy;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_adc(state->A, state->C, state->flags.cy);
            state->A = result & FIRST_8_BITS; 
        break;
        case 0x8a:  // ADC D - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->D + (uint16_t) state->flags.cy;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_adc(state->A, state->D, state->flags.cy);
            state->A = result & FIRST_8_BITS;
        break;
        case 0x8b:  // ADC E - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->E + (uint16_t) state->flags.cy;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_adc(state->A, state->E, state->flags.cy);
            state->A = result & FIRST_8_BITS; 
        break;
        case 0x8c:  // ADC H - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->H + (uint16_t) state->flags.cy;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_adc(state->A, state->H, state->flags.cy);
            state->A = result & FIRST_8_BITS; 
        break;
        case 0x8d:  // ADC L - 4 cykle
            result = (uint16_t) state->A + (uint16_t) state->L + (uint16_t) state->flags.cy;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_adc(state->A, state->L, state->flags.cy);
            state->A = result & FIRST_8_BITS; 
        break;
        case 0x8e:  // ADC M - 7 cykli
            offset = (state->H << 8) | (state->L);
            result = (uint16_t) state->A + (uint16_t) state->memory[offset] + (uint16_t) state->flags.cy;
            state->flags.z = ((result & FIRST_8_BITS) == 0);
            state->flags.s = ((result & MSB) != 0);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.ac = check_ac_adc(state->A, state->memory[offset], state->flags.cy);
            state->A = result & FIRST_8_BITS;
        break;
        case 0x8f:  // ADC A - 4 cykle 
            result = (uint16_t) state->A + (uint16_t) state->A + (uint16_t) state->flags.cy;
            state->flags.z =  ((result & FIRST_8_BITS) == 0);
            state->flags.s =  ((result & MSB) != 0);
            state->flags.cy = (result > FIRST_8_BITS);
            state->flags.p = parity(result & FIRST_8_BITS);
            state->flags.ac = check_ac_adc(state->A, state->A, state->flags.cy);
            state->A = result & FIRST_8_BITS; 
        break;

        case 0x90: UnimplementedInstruction(state); break;
        case 0x91: UnimplementedInstruction(state); break;
        case 0x92: UnimplementedInstruction(state); break;
        case 0x93: UnimplementedInstruction(state); break;
        case 0x94: UnimplementedInstruction(state); break;
        case 0x95: UnimplementedInstruction(state); break;
        case 0x96: UnimplementedInstruction(state); break;
        case 0x97: UnimplementedInstruction(state); break;
        case 0x98: UnimplementedInstruction(state); break;
        case 0x99: UnimplementedInstruction(state); break;
        case 0x9a: UnimplementedInstruction(state); break;
        case 0x9b: UnimplementedInstruction(state); break;
        case 0x9c: UnimplementedInstruction(state); break;
        case 0x9d: UnimplementedInstruction(state); break;
        case 0x9e: UnimplementedInstruction(state); break;
        case 0x9f: UnimplementedInstruction(state); break;

        case 0xa0: UnimplementedInstruction(state); break;
        case 0xa1: UnimplementedInstruction(state); break;
        case 0xa2: UnimplementedInstruction(state); break;
        case 0xa3: UnimplementedInstruction(state); break;
        case 0xa4: UnimplementedInstruction(state); break;
        case 0xa5: UnimplementedInstruction(state); break;
        case 0xa6: UnimplementedInstruction(state); break;
        case 0xa7: UnimplementedInstruction(state); break;
        case 0xa8: UnimplementedInstruction(state); break;
        case 0xa9: UnimplementedInstruction(state); break;
        case 0xaa: UnimplementedInstruction(state); break;
        case 0xab: UnimplementedInstruction(state); break;
        case 0xac: UnimplementedInstruction(state); break;
        case 0xad: UnimplementedInstruction(state); break;
        case 0xae: UnimplementedInstruction(state); break;
        case 0xaf: UnimplementedInstruction(state); break;

        case 0xb0: UnimplementedInstruction(state); break;
        case 0xb1: UnimplementedInstruction(state); break;
        case 0xb2: UnimplementedInstruction(state); break;
        case 0xb3: UnimplementedInstruction(state); break;
        case 0xb4: UnimplementedInstruction(state); break;
        case 0xb5: UnimplementedInstruction(state); break;
        case 0xb6: UnimplementedInstruction(state); break;
        case 0xb7: UnimplementedInstruction(state); break;
        case 0xb8: UnimplementedInstruction(state); break;
        case 0xb9: UnimplementedInstruction(state); break;
        case 0xba: UnimplementedInstruction(state); break;
        case 0xbb: UnimplementedInstruction(state); break;
        case 0xbc: UnimplementedInstruction(state); break;
        case 0xbd: UnimplementedInstruction(state); break;
        case 0xbe: UnimplementedInstruction(state); break;
        case 0xbf: UnimplementedInstruction(state); break;

        case 0xc0: UnimplementedInstruction(state); break;
        case 0xc1: UnimplementedInstruction(state); break;
        case 0xc2:  // JNZ A16 - 10 cykli    
            if (state->flags.z == 0)
                    state->PC = (opcode[2] << 8) | opcode[1];
            else
                state->PC += 2;             
        break;
        case 0xc3:  // JMP A16 - 10 cykli
            offset = (opcode[2] << 8) | opcode[1];
            state->PC = offset;
        break;
        case 0xc4: UnimplementedInstruction(state); break;
        case 0xc5: UnimplementedInstruction(state); break;
        case 0xc6: UnimplementedInstruction(state); break;
        case 0xc7: UnimplementedInstruction(state); break;
        case 0xc8: UnimplementedInstruction(state); break;
        case 0xc9:  // RET - 10 cykli
            state->PC = state->memory[state->SP] | (state->memory[state->SP+1] << 8);
            state->SP += 2;
        break;
        case 0xca: UnimplementedInstruction(state); break;
        case 0xcb: UnimplementedInstruction(state); break;
        case 0xcc: UnimplementedInstruction(state); break;
        case 0xcd:  // CALL A 16 - 17 cykli
        {
			uint16_t ret = state->PC + 2;
			state->memory[state->SP-1] = (ret >> 8) & 0xff;
			state->memory[state->SP-2] = (ret & 0xff);
			state->SP = state->SP - 2;
            state->PC = (opcode[2] << 8) | opcode[1];
		} 
        break;
        case 0xce: UnimplementedInstruction(state); break;
        case 0xcf: UnimplementedInstruction(state); break;

        case 0xd0: UnimplementedInstruction(state); break;
        case 0xd1: UnimplementedInstruction(state); break;
        case 0xd2: UnimplementedInstruction(state); break;
        case 0xd3: UnimplementedInstruction(state); break;
        case 0xd4: UnimplementedInstruction(state); break;
        case 0xd5: UnimplementedInstruction(state); break;
        case 0xd6: UnimplementedInstruction(state); break;
        case 0xd7: UnimplementedInstruction(state); break;
        case 0xd8: UnimplementedInstruction(state); break;
        case 0xd9: UnimplementedInstruction(state); break;
        case 0xda: UnimplementedInstruction(state); break;
        case 0xdb: UnimplementedInstruction(state); break;
        case 0xdc: UnimplementedInstruction(state); break;
        case 0xdd: UnimplementedInstruction(state); break;
        case 0xde: UnimplementedInstruction(state); break;
        case 0xdf: UnimplementedInstruction(state); break;

        case 0xe0: UnimplementedInstruction(state); break;
        case 0xe1: UnimplementedInstruction(state); break;
        case 0xe2: UnimplementedInstruction(state); break;
        case 0xe3: UnimplementedInstruction(state); break;
        case 0xe4: UnimplementedInstruction(state); break;
        case 0xe5: UnimplementedInstruction(state); break;
        case 0xe6: UnimplementedInstruction(state); break;
        case 0xe7: UnimplementedInstruction(state); break;
        case 0xe8: UnimplementedInstruction(state); break;
        case 0xe9: UnimplementedInstruction(state); break;
        case 0xea: UnimplementedInstruction(state); break;
        case 0xeb: UnimplementedInstruction(state); break;
        case 0xec: UnimplementedInstruction(state); break;
        case 0xed: UnimplementedInstruction(state); break;
        case 0xee: UnimplementedInstruction(state); break;
        case 0xef: UnimplementedInstruction(state); break;

        case 0xf0: UnimplementedInstruction(state); break;
        case 0xf1: UnimplementedInstruction(state); break;
        case 0xf2: UnimplementedInstruction(state); break;
        case 0xf3: UnimplementedInstruction(state); break;
        case 0xf4: UnimplementedInstruction(state); break;
        case 0xf5: UnimplementedInstruction(state); break;
        case 0xf6: UnimplementedInstruction(state); break;
        case 0xf7: UnimplementedInstruction(state); break;
        case 0xf8: UnimplementedInstruction(state); break;
        case 0xf9: UnimplementedInstruction(state); break;
        case 0xfa: UnimplementedInstruction(state); break;
        case 0xfb: UnimplementedInstruction(state); break;
        case 0xfc: UnimplementedInstruction(state); break;
        case 0xfd: UnimplementedInstruction(state); break;
        case 0xfe: UnimplementedInstruction(state); break;
        case 0xff: UnimplementedInstruction(state); break;
    }   // switch(*opcode)
    //print_state(*state);  
}

int parity (uint16_t number)
{
    int result = 0;
    int i;
    for (i = 0; i < 8 * sizeof(number); i++)
        result += ( (number & (1 << i)) != 0 );
    return !(result % 2);
}

int check_ac_add(uint16_t number1, uint16_t number2)
{
    number1 &= 15;  // Skróć wartości do czterobitowych
    number2 &= 15;
    if (number1 + number2 > 15)
        return 1;
    return 0;
}

int check_ac_adc(uint16_t number1, uint16_t number2, uint16_t carry)
{
    number1 &= 15;  // Skróć wartości do czterobitowych
    number2 &= 15;
    if (number1 + number2 + carry > 15)
        return 1;
    return 0;
}

int check_ac_sub(uint16_t number1, uint16_t number2)
{
    number1 = number1 & 15;
    number2 = number2 & 15;
    if (number1 < number2)
        return 1;
    return 0;
}

void print_state(State8080 state)
{
    printf("ACC: %02x BC: %02x%02x DE: %02x%02x HL: %02x%02x PC: %04x SP: %04x ", 
    state.A, state.B, state.C, state.D, state.E, state.H, state.L, state.PC, state.SP);
    printf("%c", state.flags.z ?  'z' : '.');
    printf("%c", state.flags.s ?  's' : '.');
    printf("%c", state.flags.p ?  'p' : '.');
    printf("%c", state.flags.cy ? 'c' : '.');
    printf("%c\n", state.flags.ac ? 'a' : '.');
}
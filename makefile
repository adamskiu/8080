OUT = 8080
CC = gcc
CFLAGS = -Wall -Werror -g -Iinclude
LDFLAGS =
SOURCES = ./src/*

all:	$(SOURCES)
	$(CC) $(SOURCES) $(CFLAGS) $(LDFLAGS) -o $(OUT)

clean:	
	rm -fv	$(OUT)

